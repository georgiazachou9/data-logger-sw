/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define EXTINT_GPS_Pin GPIO_PIN_0
#define EXTINT_GPS_GPIO_Port GPIOC
#define LED_DEBUG_GREEN_Pin GPIO_PIN_1
#define LED_DEBUG_GREEN_GPIO_Port GPIOA
#define MODULE_RX_Pin GPIO_PIN_2
#define MODULE_RX_GPIO_Port GPIOA
#define MODULE_TX_Pin GPIO_PIN_3
#define MODULE_TX_GPIO_Port GPIOA
#define CS_ALT2_Pin GPIO_PIN_4
#define CS_ALT2_GPIO_Port GPIOA
#define INT2_XM_Pin GPIO_PIN_1
#define INT2_XM_GPIO_Port GPIOB
#define INT1_XM_Pin GPIO_PIN_2
#define INT1_XM_GPIO_Port GPIOB
#define DRDY_G_Pin GPIO_PIN_10
#define DRDY_G_GPIO_Port GPIOB
#define INT_G_Pin GPIO_PIN_11
#define INT_G_GPIO_Port GPIOB
#define CS_ALT1_Pin GPIO_PIN_12
#define CS_ALT1_GPIO_Port GPIOB
#define HOLD_FLASH_Pin GPIO_PIN_13
#define HOLD_FLASH_GPIO_Port GPIOB
#define CS_SD_Pin GPIO_PIN_14
#define CS_SD_GPIO_Port GPIOB
#define CS_FLASH_Pin GPIO_PIN_6
#define CS_FLASH_GPIO_Port GPIOC
#define WP_FLASH_Pin GPIO_PIN_7
#define WP_FLASH_GPIO_Port GPIOC
#define CS_XM_Pin GPIO_PIN_9
#define CS_XM_GPIO_Port GPIOC
#define CS_G_Pin GPIO_PIN_8
#define CS_G_GPIO_Port GPIOA
#define BREAKOUT_RX_Pin GPIO_PIN_9
#define BREAKOUT_RX_GPIO_Port GPIOA
#define BREAKOUT_TX_Pin GPIO_PIN_10
#define BREAKOUT_TX_GPIO_Port GPIOA
#define BNO_INT_Pin GPIO_PIN_15
#define BNO_INT_GPIO_Port GPIOA
#define BNO_ADDR_SEL_Pin GPIO_PIN_10
#define BNO_ADDR_SEL_GPIO_Port GPIOC
#define BNO_BOOT_LOAD_Pin GPIO_PIN_11
#define BNO_BOOT_LOAD_GPIO_Port GPIOC
#define INT_BMP384_Pin GPIO_PIN_12
#define INT_BMP384_GPIO_Port GPIOC
#define BNO_RESET_Pin GPIO_PIN_2
#define BNO_RESET_GPIO_Port GPIOD
#define CAN_STB_Pin GPIO_PIN_4
#define CAN_STB_GPIO_Port GPIOB
#define CAN_EN_Pin GPIO_PIN_5
#define CAN_EN_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
