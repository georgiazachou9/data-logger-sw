/*
 * bmp384.c
 *
 *  Created on: May 28, 2022
 *  Edited: July 20, 2022 by Vic
 *      Author: thodoris
 */

#include "bmp384.h"
#include "stm32l4xx_hal.h"
#include <math.h>

extern double Temperature, Pressure;
uint8_t chipID;
uint32_t tRaw, pRaw;

   /* NVM Raw Trimming Coefficients */      
uint16_t NVM_PAR_T1, NVM_PAR_T2,NVM_PAR_P5,NVM_PAR_P6;
int16_t  NVM_PAR_P1,NVM_PAR_P2,NVM_PAR_P3,NVM_PAR_P4,NVM_PAR_T3, NVM_PAR_P7, NVM_PAR_P8, NVM_PAR_P9, NVM_PAR_P10, NVM_PAR_P11;

   /* Float Trimming Coefficients */    
double  PAR_T1, PAR_T2, PAR_T3;
double  PAR_P1,PAR_P2,PAR_P3,PAR_P4,PAR_P5;
double  PAR_P6, PAR_P7, PAR_P8, PAR_P9, PAR_P10, PAR_P11;

double comp_temp,comp_press;

extern bmp384_settings bmpSettings;
extern bmp384_driver bmpDriver;
extern bmp384_sensor bmpSensor;
extern bmp384_data bmpData;


/* -------------------------------Write Parameter Settings--------------------- */


uint8_t getModeMask(bmp384_modes mode) {
	uint8_t mode_value = 0;
	switch(mode) {
	case SLEEP:
		mode_value = BMP384_REGISTER_PWR_CTRL_MODE_SLEEP;
		break;
	case FORCED:
		mode_value = BMP384_REGISTER_PWR_CTRL_MODE_FORCED;
		break;
	case NORMAL:
		mode_value = BMP384_REGISTER_PWR_CTRL_MODE_NORMAL;
		break;
	}
	return mode_value;
}


uint8_t getChipId() {
	uint16_t reg = BMP384_REGISTER_CHIP_ID;
	uint8_t data = 0;

	bmpDriver.read(reg, 1, &data, 1);

	bmpSensor.chip_id = data;

	return bmpSensor.chip_id;
}

HAL_StatusTypeDef setPowerCtrlRegister() {

	uint8_t prev;

	bmp384_modes mode = bmpSettings.mode;
	uint8_t tempratureEn = bmpSettings.tempratureEn;
	uint8_t pressureEn = bmpSettings.pressureEn;
	uint8_t mode_value = getModeMask(mode);

	uint16_t reg = BMP384_REGISTER_PWR_CTRL;

	bmpDriver.read(reg, 1, &prev, 1);

	tempratureEn = (tempratureEn & 0x01) << BMP384_REGISTER_PWR_CTRL_TEMP_EN_BIT;
	pressureEn = (pressureEn & 0x01) << BMP384_REGISTER_PWR_CTRL_PRESS_EN_BIT;

	uint8_t value = mode_value | pressureEn | tempratureEn | prev;

	return bmpDriver.write(reg, 1, &value, 1);
}

HAL_StatusTypeDef setConfigRegister() {
	uint16_t reg = BMP384_REGISTER_CONFIG;

	uint8_t value = bmpSettings.configRegValue;
	value = value & 7; // 7 = 00000111(2)
	value <<= BMP384_REGISTER_CONFIG_IIR_FILTER_MASK;

	return bmpDriver.write(reg, 1, &value, 1);
}

HAL_StatusTypeDef setCmd(uint8_t value) {
	uint16_t reg =  BMP384_REGISTER_CMD;
	return bmpDriver.write(reg, 1, &value, 1);
}

// prescalerOption can be BMP384_REGISTER_ODR_SEL_ODR_50 for example
HAL_StatusTypeDef setODRRegister() {
	uint16_t reg = BMP384_REGISTER_ODR;

	uint8_t prescalerOption = bmpSettings.odr_select;

	if (prescalerOption > 0x11)
		prescalerOption = 0x11;

	return bmpDriver.write(reg, 1, &prescalerOption, 1);
}

HAL_StatusTypeDef setOSRRegister() {

	uint16_t reg = BMP384_REGISTER_OSR;
	uint8_t osr_p = bmpSettings.osr_p & 0x7;
	uint8_t osr_t = bmpSettings.osr_t & 0x38;
	uint8_t prev;

	bmpDriver.read(reg, 1, &prev, 1);

	uint8_t data = osr_p | osr_t | prev;

	return bmpDriver.write(reg, 1, &data, 1);
}

HAL_StatusTypeDef setFifoWtm() {
	// TODO: Do it with 1 write command

	uint16_t reg = BMP384_REGISTER_FIFO_WTM_0;
	uint8_t prev;

	HAL_StatusTypeDef res = bmpDriver.read(reg, 1, &prev, 1);
	uint8_t data = (bmpSettings.fifoWaterMark & 0xff) | prev;

	res |= bmpDriver.write(reg, 1, &data, 1);

	reg = BMP384_REGISTER_FIFO_WTM_1;

	res |= bmpDriver.read(reg, 1, &prev, 1);
	data = ((bmpSettings.fifoWaterMark >> 8) & 0x01) | prev;

	res |= bmpDriver.write(reg, 1, &data, 1);

	return res;
}

HAL_StatusTypeDef setFifoConfRegisters() {
	uint16_t reg = BMP384_REGISTER_FIFO_CONFIG_1;
	uint8_t data = bmpSettings.config1;

	HAL_StatusTypeDef res = bmpDriver.write(reg, 1, &data, 1);

	reg = BMP384_REGISTER_FIFO_CONFIG_2;
	data = bmpSettings.config2;

	res |= bmpDriver.write(reg, 1, &data, 1);

	return res;
	// TODO: Do it with 1 write command
}

HAL_StatusTypeDef setIntCtrlRegister() {
	uint16_t reg = BMP384_REGISTER_INT_CTRL;
	uint8_t data = bmpSettings.int_control;

	return bmpDriver.write(reg, 1, &data, 1);
}
HAL_StatusTypeDef setIfConfRegister() {
	uint16_t reg = BMP384_REGISTER_IF_CONF;
	uint8_t data = bmpSettings.if_conf;


	return bmpDriver.write(reg, 1, &data, 1);
}


/* ------------------------------- Read Data --------------------- */

uint8_t getStatusFlags() {
	uint16_t reg = BMP384_REGISTER_STATUS;
	uint8_t data = 0;

	bmpDriver.read(reg, 1, &data, 1);

	bmpSensor.cmd_rdy = (data >> BMP384_REGISTER_STATUS_CMD_RDY_BIT) & 0x01;
	bmpSensor.drdy_press = (data >> BMP384_REGISTER_STATUS_DRDY_PRESS_BIT) & 0x01;
	bmpSensor.drdy_temp = (data >> BMP384_REGISTER_STATUS_DRDY_TEMP_BIT) & 0x01;

	return data;

}

uint8_t getErrorStatus() {
	uint16_t reg = BMP384_REGISTER_ERR_REG;
	uint8_t data = 0;

	bmpDriver.read(reg, 1, &data, 1);

	bmpSensor.cmd_err = (data >> BMP384_REGISTER_ERR_REG_CMD_ERR_BIT) & 0x01;
	bmpSensor.conf_err = (data >> BMP384_REGISTER_ERR_REG_CONF_ERR_BIT) & 0x01;
	bmpSensor.fatal_err = (data >> BMP384_REGISTER_ERR_REG_FATAL_ERR_BIT) & 0x01;

	return data & 0x07;
}

uint8_t getEventRegister() {
	uint16_t reg = BMP384_REGISTER_EVENT;
	uint8_t data = 0;

	bmpDriver.read(reg, 1, &data, 1);

	bmpSensor.por_detected = data & 0x01;

	return data;
}

uint8_t getIntStatusRegister() {
	uint16_t reg = BMP384_REGISTER_INT_STATUS;
	uint8_t data = 0;

	bmpDriver.read(reg, 1, &data, 1);

	bmpSensor.drdy = (data >> BMP384_REGISTER_INT_STATUS_DRDY_BIT) & 0x01;
	bmpSensor.ffull_int = (data >> BMP384_REGISTER_INT_STATUS_FFUL_INT_BIT) & 0x01;
	bmpSensor.fwm_int = (data >> BMP384_REGISTER_INT_STATUS_FWM_INT_BIT) & 0x01;

	return data;
}

void setSettings() {
	// Filter Selection: Drone (p.16)
	// PWR_CTRL
	bmpSettings.mode = NORMAL;
	bmpSettings.pressureEn = 1;
	bmpSettings.tempratureEn = 1;

	// FIFO_WTM_1 & FIFO_WTM_1
	bmpSettings.fifoWaterMark = 0x01;

	// OSR
	bmpSettings.osr_p = BMP384_REGISTER_OSR_OSR_P_3; // resolution x8
	bmpSettings.osr_t = BMP384_REGISTER_OSR_OSR_T_0; // resolution x1

	// ODR ( Prescaler: 4, ODR 50Hz, Sampling time: 20 ms)
	bmpSettings.odr_select = BMP384_REGISTER_ODR_SEL_ODR_50;

	// CONFIG
	bmpSettings.configRegValue = BMP384_REGISTER_CONFIG_IIR_FILTER_COEF_3;

	// CMD
	bmpSettings.nop = BMP384_REGISTER_CMD_NOP;
	bmpSettings.softreset = BMP384_REGISTER_CMD_SOFTRESET;

	// FIFO_CONFIG_1 & FIFO_CONFIG_2
	bmpSettings.config1 =
			  (1 << BMP384_REGISTER_FIFO_CONFIG_1_FIFO_MODE_BIT)
			| (0 << BMP384_REGISTER_FIFO_CONFIG_1_FIFO_STOP_ON_FULL_BIT)
			| (1 << BMP384_REGISTER_FIFO_CONFIG_1_FIFO_TIME_EN_BIT)
			| (1 << BMP384_REGISTER_FIFO_CONFIG_1_FIFO_PRESS_EN_BIT)
			| (1 << BMP384_REGISTER_FIFO_CONFIG_1_FIFO_TEMP_EN_BIT);

	bmpSettings.config2 =
			(1 << BMP384_REGISTER_FIFO_CONFIG_2_DATA_SELECT_MASK)
			| (2 << BMP384_REGISTER_FIFO_CONFIG_2_FIFO_SUBSAMPLING_MASK);

	// INT_CTRL
	bmpSettings.int_control =
			(0 << BMP384_REGISTER_INT_CTRL_INT_OD_BIT)
			| (1 << BMP384_REGISTER_INT_CTRL_INT_LEVEL_BIT)
			| (0 << BMP384_REGISTER_INT_CTRL_INT_LATCH_BIT)
			| (0 << BMP384_REGISTER_INT_CTRL_FWTM_EN_BIT)
			| (0 << BMP384_REGISTER_INT_CTRL_FFULL_EN_BIT)
			| (0 << BMP384_REGISTER_INT_CTRL_DRDY_EN_BIT);

	// IF_CONF (I2C watchdog timeout after 40 ms)
	bmpSettings.if_conf = (0 << BMP384_REGISTER_IF_CONF_SPI3_BIT)
			| (0 << BMP384_REGISTER_IF_CONF_I2C_WDT_EN_BIT)
			| (0 << BMP384_REGISTER_IF_CONF_I2C_WDT_SEL_BIT);
}

// Read NVM Registers 
void TrimRead(void)
{
	uint8_t trimdata[21];
	// Read NVM from 0x21 to 0x45
	bmpDriver.read(0x31, 1, trimdata, 21); // shows interrupt status
	// Arrange the data as per the datasheet (page no. 24)
	NVM_PAR_T1 = (trimdata[1]<<8) | trimdata[0];
	NVM_PAR_T2 = (trimdata[3]<<8) | trimdata[2];
	NVM_PAR_T3 = trimdata[4];
	NVM_PAR_P1 = (trimdata[6]<<8) | trimdata[5];
	NVM_PAR_P2 = (trimdata[8]<<8) | trimdata[7];
	NVM_PAR_P3 = trimdata[9];
	NVM_PAR_P4 = trimdata[10];
	NVM_PAR_P5 = (trimdata[12]<<8) | trimdata[11];
	NVM_PAR_P6 = (trimdata[14]<<8) | trimdata[13];
	NVM_PAR_P7 = trimdata[15];
	NVM_PAR_P8 = trimdata[16];
	NVM_PAR_P9 = (trimdata[18]<<8) | trimdata[17];
	NVM_PAR_P10 = trimdata[19];
	NVM_PAR_P11 = trimdata[20];
	return;
}



void CalibCoeffs2Float(void)
{
	PAR_T1 = ((double) NVM_PAR_T1) / 0.00390625f ;
	PAR_T2 = ((double) NVM_PAR_T2) / 1073741824.0f;
	PAR_T3 = ((double)NVM_PAR_T3) / 281474976710656.0f;
	PAR_P1 = (((double)NVM_PAR_P1) - 16384) / 1048576.0f;
	PAR_P2 = (((double)NVM_PAR_P2) - 16384) / 536870912.0f;
	PAR_P3 = ((double)NVM_PAR_P3) /  4294967296.0f;
	PAR_P4 = ((double)NVM_PAR_P4) / 137438953472.0f;
	PAR_P5 = ((double)NVM_PAR_P5) / 0.125f;
	PAR_P6 = ((double)NVM_PAR_P6) / 64.0f;
	PAR_P7 = ((double)NVM_PAR_P7) /  256.0f;
	PAR_P8 = ((double)NVM_PAR_P8) /  32768.0f;
	PAR_P9 = ((double)NVM_PAR_P9) / 281474976710656.0f;
	PAR_P10 = ((double)NVM_PAR_P10) / 281474976710656.0f;
	PAR_P11 = ((double)NVM_PAR_P11) / 36893488147419103232.0f;
	return;
}

void BMP384_compensate_temperature()
{
	uint32_t uncomp_temp = bmpData.raw_temperature;

	double partial_data1;
	double partial_data2;
	partial_data1 = (double)(uncomp_temp - PAR_T1);
	partial_data2 = (double)(partial_data1 * PAR_T2);
	/* Update the compensated temperature in calib structure since this is
	* needed for pressure calculation */
	comp_temp = partial_data2 + (partial_data1 * partial_data1) * PAR_T3;
	/* Returns compensated temperature */

	bmpData.temperature = comp_temp;
}


void BMP384_compensate_pressure()
{
	uint32_t uncomp_press = bmpData.raw_pressure;

	/* Temporary variables used for compensation */
	double partial_data1;
	double partial_data2;
	double partial_data3;
	double partial_data4;
	double partial_out1;
	double partial_out2;

	/* Calibration data */
	partial_data1 = PAR_P6 * comp_temp;
	partial_data2 = PAR_P7 * (comp_temp * comp_temp);
	partial_data3 = PAR_P8 * (comp_temp * comp_temp * comp_temp);
	partial_out1 = PAR_P5 + partial_data1 + partial_data2 + partial_data3;
	partial_data1 = PAR_P2 * comp_temp;
	partial_data2 = PAR_P3 * (comp_temp * comp_temp);
	partial_data3 = PAR_P4 * (comp_temp * comp_temp * comp_temp);
	partial_out2 = (double)uncomp_press * (PAR_P1 + partial_data1 + partial_data2 + partial_data3);
	partial_data1 = (double)uncomp_press * (double)uncomp_press;
	partial_data2 = PAR_P9 + PAR_P10 * comp_temp;
	partial_data3 = partial_data1 * partial_data2;
	partial_data4 = partial_data3 + ((double)uncomp_press * (double)uncomp_press * (double)uncomp_press) * PAR_P11;
	comp_press = partial_out1 + partial_out2 + partial_data4;

	bmpData.pressure = comp_press;
}


void BMPReadRawData(void)
{
	uint8_t RawData[6];

	// Read the Registers 0x04 to 0x09
	bmpDriver.read(BMP384_REGISTER_DATA0, 1, RawData, 6); // shows interrupt status

	/* Calculate the Raw data for the parameters
	 * Here the Pressure and Temperature are in 24 bit format
	 */
	bmpData.raw_pressure 	= (RawData[2]<<16) | (RawData[1]<<8) | RawData[0];
	bmpData.raw_temperature = (RawData[5]<<16) | (RawData[4]<<8) | RawData[3];
}


void BMP384_Measure ()
{
	BMPReadRawData();

	BMP384_compensate_temperature();
	BMP384_compensate_pressure();
}



HAL_StatusTypeDef initializeBmp384() {
	setCmd(0xB6);
	HAL_StatusTypeDef res = HAL_OK;

	if(getChipId() != 0x50)
		return HAL_ERROR;

	setSettings();

//	res |= setFifoWtm();
	res |= setFifoConfRegisters();
	res |= setIntCtrlRegister();
	res |= setIfConfRegister();

//	res |= setOSRRegister();

	res |= setODRRegister();
	res |= setConfigRegister();

	res |= setPowerCtrlRegister();

	uint8_t error = getErrorStatus();
	if (error) return HAL_ERROR;

	return res;
}
