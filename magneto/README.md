# Calibration Procedure

This repo includes a general method for calibrating magnetometer & accelerometer using python scipts and the magneto software.

[Magneto download link](https://sites.google.com/site/sailboatinstruments1/home)

## General Information
The LSM9DS0 includes a 3D accelerometer, 3D gyroscope and 3D magnetometer. In this project, it communicates through SPI serial interface and it can provide information such as acceleration, magnetic field, angular rate, orientation/heading and possibly rotation angle.

## [Accelerometer]()


## [Magnetometer]()
Inside the **Magnetometer** folder, there are:
- a [magnetometer_raw_data]() folder, that contains the uncalibrated data in txt form.
- a [plot_images]() folder, that includes the figures of the uncalibrated & uncalibrated data
- the [read_serial_port.py]() script, to read the measurements through the serial port
- the [plot_data.py script](), to plot the uncalibrated & calibrated data.





