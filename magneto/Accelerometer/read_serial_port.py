import argparse
import serial
import keyboard

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--port', help="Port from where to read serial numbers in format num1 =  num, num2 = num, num3 = num",  required=True)
parser.add_argument('-b', '--baudrate', help="Baudrate for the specified port", default="9600")
parser.add_argument('--bytesize', help="bytesize for serial reading", default="8")
parser.add_argument("--num1", help="num1 specifier for required format", required=True)
parser.add_argument("--num2", help="num2 specifier for required format", required=True)
parser.add_argument("--num3", help="num3 specifier for required format", required=True)
parser.add_argument("-o", "--output_file", help="output text file where data will be stored", default="myoutput.txt")

args = parser.parse_args()
num1 = str(args.num1)
num2 = str(args.num2)
num3 = str(args.num3)
print(num1, num2, num3)
output = str(args.output_file)
output_file = open(args.output_file, 'a+')
output_file.truncate(0)
print(args.port)
print("inside")

serialPort = serial.Serial(port = args.port, baudrate = args.baudrate, bytesize = 8, stopbits=serial.STOPBITS_ONE)

while(1):

    if(serialPort.in_waiting > 0):
        
        serialString = serialPort.readline()
        
        serialString = str(serialString).replace("\\n", "\n").replace("'", "")
        #print(serialString)
        if(not(num1 in serialString) or not(num2 in serialString) or not(num3 in serialString)):
            continue
        values = serialString.replace(" ", "").replace("\n", "").split(",") 
        #print(values[0].split("=")[1])
        num1_value = float(values[0].split("=")[1])
        num2_value = float(values[1].split("=")[1])
        num3_value = float(values[2].split("=")[1])    
        to_write = str(num1_value) + " " + str(num2_value) + "  " + str(num3_value) + "\n"
        
        if keyboard.is_pressed('m'):
            output_file.write(to_write)
        
        print(to_write)
        
