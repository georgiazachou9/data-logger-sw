import numpy as np
import matplotlib.pyplot as plt


# Define calibration parameters
A = np.array([[1.004332, 0.000046, 0.004896],  # 'A^-1' matrix from Magneto
            [0.000046, 0.969793, 0.009452],
            [0.004896, 0.009452, 1.022384]])
# 'Combined bias (b)' vector from Magneto
b = np.array([0.027031, -0.040204, 0.046558])

# define arrays for the raw & the calibrated values
# Raw data
accel_x_y = []
accel_x_z = []
accel_y_z = []
# Calibrated data
accel_x_y_cal = []
accel_x_z_cal = []
accel_y_z_cal = []

with open('acceldata_x_y.txt') as f:
    while True:
        line = f.readline()
        if not line: 
            break
        #print(line)
        line_ = line.replace("\n", "")
        
        line_splitted = line_.split(" ")
        print(line_splitted)
        accel_x_y.append([float(line_splitted[0]), float(line_splitted[1])])
        temp_arr = np.array([float(line_splitted[0])-b[0], float(line_splitted[1])-b[1], float(line_splitted[3])-b[2]]).reshape((3,1))
        # print(temp_arr.shape)
        mat_mul = np.matmul(A, temp_arr)
        print("mat_mul =", mat_mul)

        accel_x_y_cal.append([mat_mul[0], mat_mul[1]])

with open('acceldata_x_z.txt') as f:
    while True:
        line = f.readline()
        if not line: 
            break
        #print(line)
        line_ = line.replace("\n", "")
        #line__ = line.replace(" ","  ") 
        line_splitted = line_.split(" ")
        print(line_splitted)
        accel_x_z.append([float(line_splitted[0]), float(line_splitted[1])])
        temp_arr = np.array([float(line_splitted[0])-b[0], float(line_splitted[1])-b[1], float(line_splitted[3])-b[2]]).reshape((3,1))
        # print(temp_arr.shape)
        mat_mul = np.matmul(A, temp_arr)
        print("mat_mul =", mat_mul)
        
        accel_x_z_cal.append([mat_mul[0], mat_mul[2]])

with open('acceldata_y_z.txt') as f:
    while True:
        line = f.readline()
        if not line: 
            break
        #print(line)
        line_ = line.replace("\n", "")
        #line__ = line.replace(" ","  ") 
        line_splitted = line_.split(" ")
        print(line_splitted)
        accel_y_z.append([float(line_splitted[0]), float(line_splitted[1])])
        temp_arr = np.array([float(line_splitted[0])-b[0], float(line_splitted[1])-b[1], float(line_splitted[3])-b[2]]).reshape((3,1))
        # print(temp_arr.shape)
        mat_mul = np.matmul(A, temp_arr)
        print("mat_mul =", mat_mul)
        
        accel_y_z_cal.append([mat_mul[1], mat_mul[2]])

accel_x_y = np.array(accel_x_y)
accel_x_z = np.array(accel_x_z)
accel_y_z = np.array(accel_y_z)

accel_x_y_cal = np.array(accel_x_y_cal)
accel_x_z_cal = np.array(accel_x_z_cal)
accel_y_z_cal = np.array(accel_y_z_cal)

print(accel_x_y.shape, accel_x_y_cal.shape)
print(accel_x_y_cal)
print(accel_y_z.shape)

# Figure No1
fig, axes = plt.subplots(1)#, figsize = (20, 16))
plt.plot(accel_x_y[:, 0], accel_x_y[:, 1], "r*", label="uncalibrated X-Y")
plt.plot(accel_x_y_cal[:, 0], accel_x_y_cal[:, 1], "b*", label="calibrated X-Y")
plt.legend()
plt.xlabel("X axis accelerometer data")
plt.ylabel("Y axis accelerometer data")
plt.title("X-Y accelerometer figure")
plt.axis('equal')

# Figure No2
fig, axes = plt.subplots(1)#, figsize = (20, 16))
plt.plot(accel_x_z[:, 0], accel_x_z[:, 1], "r*", label="uncalibrated X-Z")
plt.plot(accel_x_z_cal[:, 0], accel_x_z_cal[:, 1], "b*", label="calibrated X-Z")
plt.legend()
plt.xlabel("X axis accelerometer data")
plt.ylabel("Z axis accelerometer data")
plt.title("X-Z accelerometer figure")
plt.axis('equal')

# Figure No3
fig, axes = plt.subplots(1)#, figsize = (20, 16))
plt.plot(accel_y_z[:, 0], accel_y_z[:, 1], "r*", label="uncalibrated Y-Z")
plt.plot(accel_y_z_cal[:, 0], accel_y_z_cal[:, 1], "b*", label="calibrated Y-Z")
plt.legend()
plt.xlabel("Y axis accelerometer data")
plt.ylabel("Z axis accelerometer data")
plt.title("Y-Z accelerometer figure")
plt.axis('equal')

plt.show()
